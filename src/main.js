import { createApp } from 'vue'
import App from './App.vue'
// import createStore from './store/index'
import './assets/css/bootstrap.min.css'
import './assets/js/bootstrap.min.js'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { createStoreApp } from './store/index'

library.add(fas)
window.$ = window.jQuery = require('jquery');

const init = async () => {
    const module = await import('./router')
    const router = await module.default
    createApp(App)
        .use(router)
        .use(createStoreApp())
        .component('fa', FontAwesomeIcon)
        .mount('#app')
}

init()
