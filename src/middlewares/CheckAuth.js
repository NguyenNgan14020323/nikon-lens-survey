const checkAuth = (to, next, isAuthenticated) => {
  if (to.matched.some(record => record.meta.requiresAuth) && !isAuthenticated) next({ name: 'Login' })
  else next()
}

export default checkAuth