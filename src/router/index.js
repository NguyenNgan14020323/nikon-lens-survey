import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue'
import Login from '../views/Login.vue'
import Questionaire from '../views/Questionaire.vue'
import LensSimulations from '../views/LensSimulations.vue'

import Introduce from '../modules/questionaire/components/Introduce.vue'
import checkAuth from '../middlewares/CheckAuth'
const isAuthenticated = false

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/questionaire',
    name: 'Questionaire',
    component: Questionaire,
    children: [
      {
        path: 'introduce',
        component: Introduce
      }
    ]
  },
  {
    path: '/lens-simulations',
    name: 'LensSimulations',
    component: LensSimulations,
    meta: { requiresAuth: true }
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

router.beforeEach((to, from, next) => {
  checkAuth(to, next, isAuthenticated)
})

export default router