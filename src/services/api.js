class BaseApiService {

  baseUrl = "https://jsonplaceholder.typicode.com"

  resource

  constructor(resource) {
    if (!resource) throw new Error("Resource is not provided")
    this.resource = resource
  }

  getUrl(id = "") {
    return `${this.baseUrl}/${this.resource}/${id}`
  }

  handleErrors(err) {
    console.log({ message: "Error is handled here", err })
  }
}

export class ReadOnlyApiService extends BaseApiService {

  constructor(resource) {
    super(resource)
  }

  async fetch(config = {}) {
    try {
      const response = await this.fetch(this.getUrl, config)
      return await response.json()
    } catch (err) {
      console.log("err", err)
    }
  }

  async get(id) {
    try {
      console.log("id", id)
    } catch (err) {
      console.log("id", err)
    }
  }
}