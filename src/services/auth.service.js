import axios from 'axios'
import { ResponseWrapper, ErrorWrapper } from './util'
import { API_URL } from '../.env'

export class AuthService {
    static async makeLogin(passcode) {
        try {
            const res = await axios.get(API_URL + 'todos/' + passcode)
            return new ResponseWrapper(res, res.data)
        } catch (err) {
            throw new ErrorWrapper(err)
        }
    }
}
