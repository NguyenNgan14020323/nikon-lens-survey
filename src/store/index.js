// store.js
const Vue = require('vue')
import { createStore } from 'vuex'
import { ReadOnlyApiService } from '../services/api'

export function createStoreApp() {
  return createStore({
    state: () => ({
      items: {}
    }),

    actions: {
      fetchItem({ commit }, id) {
        return ReadOnlyApiService(id).then(item => {
          commit('setItem', { id, item })
        })
      }
    },

    mutations: {
      setItem(state, { id, item }) {
        Vue.set(state.items, id, item)
      }
    }
  })
}